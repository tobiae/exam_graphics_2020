#include <game_objects.h>
#include <global_game_variables.h>

#include <stb_image.h>

#define M_PI 3.1415926535897932384626433832795

// used for storing mapData of gjovik
std::vector<std::vector<float>> map;

// amount of objects to spawn for the world
int rows = 43, columns = 43;

//Initialize() Initializes everything that needs to be initialized:
void GameObjects::initializeEverything()
{
	// Finds height based on brightness of gjovik map
	initializeHeightMapData();

	initiateMeshes();

	//Player [0]
	initializeGameObject(nullptr, Transform{ glm::vec3(4.0f, 30.0f, 10.f), glm::vec3(0.3f, 0.03f, 0.3f), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(1.f, 1.f, 1.f) }, &world, Invisible);
	objects[0]->setRigidbody(Square::RIGITYPE::Player);
	// Player ground checker
	initializeGameObject(nullptr, Transform{ glm::vec3(4.0f, 30.f, 10.f), glm::vec3(0.1f, 0.01f, 0.1f), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(1.f, 1.f, 1.f) }, &world, Invisible);
	objects[1]->setRigidbody(Square::RIGITYPE::Ray);

	// initiates most of the objects used in the program
	objectsCreate();


	player.initialize(&gameCamera,
		*&objects[0],
		*&objects[1],
		&world
	);

}

void GameObjects::initializeHeightMapData()
{

	int	width = 256,
		height = 256,
		bitdepth = 4;

	unsigned char* image = stbi_load("resources/levels/Gjovik_Height MapLow.png", &width, &height, &bitdepth, 0);


	if (!image)
	{
		GFX_ERROR("Failed to load heightmap image\n");
		return;
	}

	map = std::vector<std::vector<float>>(height / 25, std::vector<float>(width / 25));

	for (int y = 0; y < height/25; y++)			// runs 64~ times
	{
		//printf("\n");
		for (int x = 0; x < width/25; x++)		// runs 64~ times
		{
			unsigned char* pixel = image + ((y * height * 25) + (x * 25));

			float pixelChunks = 0.0f;
			for (int i = 0; i < 25; i++)
			{
				for (int j = 0; j < 25; j++)
				{
					pixelChunks += pixel[(i * height) + (j)];
				}
			}
			//printf("\ny: %d x: %d : %f ", y, x, (pixelChunks/(16*16)));
			map[y][x] = pixelChunks / (25 * 25);
		}
	}

	stbi_image_free(image);
}

void GameObjects::objectsCreate()
{

	// rotating sun
	initializeGameObject(nullptr, Transform{ glm::vec3(0.0f, 20.f, 0.f), glm::vec3(0.2f), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(1.f, 1.f, 1.f) }, &world, Invisible);     // Minecraft block one
	objects[2]->setRigidbody(Square::Dynamic);

	// Pine tree object
	initializeGameObject(nullptr, Transform{ glm::vec3(0.0f, 7.f, 0.f), glm::vec3(0.025f, 0.03f, 0.025f), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(1.f, 1.f, 1.f) }, &world, Static);     // Minecraft block one
	objects[3]->setMeshID(&this->meshHandles[3]);

	// fish
	initializeGameObject(nullptr, Transform{ glm::vec3(10.0f, 12.f, 40.f), glm::vec3(0.075f, .1f, 0.1f), glm::vec3(1.0f, 0.f, 0.f), 270, glm::vec3(1.f, 1.f, 1.f) }, &world, Static);     // Minecraft block one
	objects[4]->setMeshID(&this->meshHandles[4]);

	// Creates 43 (rows) x 43 (columns) amount of objects, 
	// and assigns them a texture and height based on heightmap data
	for (int x = 0; x < rows; x++)
	{
		for (int z = 0; z < columns; z++)
		{
			//printf("\ny: %d x: %d : %f ", x, z, (map[x][z]));
			initializeGameObject(nullptr, Transform{ glm::vec3(x*3-(rows), map[x][z] / 127.0f * 20.0f, z*3 - (columns)), glm::vec3(1.5f, map[x][z] / 127.0f * 20.0f, 1.5f), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(1.f, 1.f, 1.f) }, &world, Static);     // Minecraft block one
			
			// snow texture on the top
			if (objects[objects.size() - 1]->transform.scale.y > 16.0f)
			{
				objects[objects.size() - 1]->setMeshID(&this->meshHandles[2]);
			}
			// rocky texture
			else if (objects[objects.size() - 1]->transform.scale.y > 12.0f)
			{
				objects[objects.size() - 1]->setMeshID(&this->meshHandles[1]);
			}
			// water texture
			else if (objects[objects.size() - 1]->transform.scale.y < 5.5f)
			{
				objects[objects.size() - 1]->setNewPosition(glm::vec3(x * 3 - (rows), 5.5f, z * 3 - (columns)));
				objects[objects.size() - 1]->setScale(glm::vec3(1.5f, 5.5f, 1.5f));
				objects[objects.size() - 1]->setMeshID(&this->meshHandles[5]);
			}
			// grass texture
			else
			{
				objects[objects.size() - 1]->setMeshID(&this->meshHandles[0]);
			}																																																												  //objects[objects.size()-1]->setRigidbody(Square::Dynamic);
		}
	}
}

void GameObjects::initializeGameObject(int* ID, Transform tr, GameObject* par, State s)
{
	element = new GameObject(ID, tr, par, s);
	//element->transform = transforms.getTransform(element->getMeshID());
	objects.push_back(element);
}


void GameObjects::initiateMeshList()
{
	totAmountOfMeshes = 6;
	//for each int vector it tells which meshes should be assinged this texture:
	//first textures shall be shader by mesh 0 and 1, 
	texturesFileNames = { "grass.jpg", "rough_rock.jpg", "snow.jpg", "lushPine.png", "fish.jpg", "water.jpg" };
	modelFileNames = { "untitled.obj", "scrubPine.obj", "fish.obj" };
	vertexShaderFileNames = { "sprite_vertex.vert" };
	fragmentShaderFileNames = { "sprite_fragment.frag" };


	//while the third mesh will have the last texture alone:
	meshesToAssignTexture = { {0}, {1}, {2}, {3}, {4}, {5} };
	//first model shall be shader by mesh 0 and 1, 
	//while the third mesh will have the last model alone:
	meshesToAssignModels = { {0, 1, 2, 5}, {3}, {4} };
	//all the meshes will be sharing the same shader program:
	meshesToAssignShaders = { {0,1,2, 3, 4, 5} };
}

void GameObjects::initiateMeshes()
{

	std::cout << "hei1\n";
	this->initiateMeshList();

	for (int h = 0; h < totAmountOfMeshes; h++)
	{
		mesh.addMesh(&this->meshHandles[h]);
	}

	//Assigning meshes a model/renderables:
	for (int i = 0; i < meshesToAssignModels.size(); i++)
	{
		//Reserving a pointer in mesh to then be give to the renderer:
		auto ID = this->mesh.allocateModelID();
		//Loading model:
		gameRenderer.addRenderable(ID, ("resources/objects/" + modelFileNames[i]).c_str());
		//Looping through all the meshes which is assigned to this model:
		for (int j = 0; j < meshesToAssignModels[i].size(); j++)
		{
			//Assigns this meshes ID pointer to the ID recieved from the renderer:
			mesh.meshDatas[meshesToAssignModels[i][j]].virtualModelID = ID;
		}
	}
	//Assigning meshes a texture:
	for (int i = 0; i < meshesToAssignTexture.size(); i++)
	{
		//Reserving a pointer in mesh to then be give to the renderer:
		auto ID = this->mesh.allocateTextureID();
		//Loading model:
		gameRenderer.addTexture(ID, "resources/textures/" + texturesFileNames[i], GL_REPEAT, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST);
		//Looping through all the meshes which is assigned to this model:
		for (int j = 0; j < meshesToAssignTexture[i].size(); j++)
		{
			//Assigns this meshes ID pointer to the ID recieved from the renderer:
			mesh.meshDatas[meshesToAssignTexture[i][j]].virtualTextureID = ID;
		}
	}
	//Assigning meshes a shader:
	for (int i = 0; i < meshesToAssignShaders.size(); i++)
	{
		//Reserving a pointer in mesh to then be give to the renderer:
		auto ID = this->mesh.allocateShaderID();
		//Loading model:
		gameRenderer.addShader(ID, ("resources/shaders/" + vertexShaderFileNames[i]).c_str(), ("resources/shaders/" + fragmentShaderFileNames[i]).c_str());
		//Looping through all the meshes which is assigned to this model:
		for (int j = 0; j < meshesToAssignShaders[i].size(); j++)
		{
			//Assigns this meshes ID pointer to the ID recieved from the renderer:
			mesh.meshDatas[meshesToAssignShaders[i][j]].virtualShaderID = ID;
		}
	}
}

void GameObjects::update(GLFWwindow* gameWindow, GLdouble deltaTime, GLdouble time)
{

	this->gameCamera.processInput(gameWindow, deltaTime);
	this->gameRenderer.setCamera(this->gameCamera.getCameraPositionVector(), this->gameCamera.getCameraTargetVector(), this->gameCamera.getCameraUpVector());

	objects[2]->setNewPosition(glm::vec3(glm::cos((time)/2) * 50.0f, glm::sin((time) / 2) * 50.0f, 0.0f));


	//Player
	player.update(gameWindow, deltaTime);
	for (Platform* child : platforms) child->update(deltaTime);
	for (Rigidbody* child : rigis) child->update(deltaTime);
	// obj.update and collision

}

// keeps track of the sun-lighting

int lightHandle1{};

void GameObjects::render(GLdouble timeSinceLastFrame, GLdouble time)
{
	this->gameRenderer.addLight(&lightHandle1, &objects[2]->transform.position, glm::vec3(0.2f, 0.5f, 0.8f), glm::vec3(0.9f, 0.4f, 0.6f), glm::vec3(0.9f, 0.8f, 0.9f), 0.25, 0.5, 0.125);

	for (GameObject* child : world.children)
	{
		switch (child->state)
		{
		case Static:
			renderStaticObjects(child);
			break;
		default:
			renderObjects(child, timeSinceLastFrame);
			break;
		}
		//renderStaticObjects(child, timeSinceLastFrame);
	}

	this->gameRenderer.drawScene();

	this->gameRenderer.removeLight(lightHandle1);

}

void GameObjects::renderStaticObjects(GameObject* obj)
{
	auto meshToRender = mesh.getMesh(obj->getMeshID());
	gameRenderer.addToScene(*meshToRender->virtualModelID, *meshToRender->virtualTextureID, *meshToRender->virtualShaderID, &obj->transform.position,
		&obj->transform.scale, &obj->transform.rotate, &obj->transform.rotationAxis, &obj->transform.color);
}

void GameObjects::renderObjects(GameObject* obj, GLdouble time)
{
	if (obj->HasVelocity() || obj->cur == Square::Ray)
	{

		for (GameObject* child : world.children)
		{
			if (child != obj) obj->IntersectWithObject(child);
		}
	}

	obj->update(time);

	if (obj->state != Invisible)
	{
		auto meshToRender = mesh.getMesh(obj->getMeshID());
		gameRenderer.addToScene(*meshToRender->virtualModelID, *meshToRender->virtualTextureID, *meshToRender->virtualShaderID, &obj->transform.position,
			&obj->transform.scale, &obj->transform.rotate, &obj->transform.rotationAxis, &obj->transform.color);
	}

	for (GameObject* child : obj->children) renderObjects(child, time);
}
