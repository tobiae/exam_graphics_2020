#include <game_application.h>



int main() {

    //create game:

    
    auto game = GameApplication();
    //run preparations for loading the objects
    game.gamePrepareLoop();
    //run game loop:
    game.gameRunLoop();
    return 0;
    
}


    