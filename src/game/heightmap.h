
// OpenGL
#include <glm/glm.hpp>
#include <glm/trigonometric.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>

// Loading Shader
#include <renderer_shader.h>

// C++
#include <vector>

// keeps track of the current matrix 
struct Matrices
{
	glm::mat4* projectionMatrix; 
	glm::mat4* orthoMatrix; 
	glm::mat4* viewMatrix; 

	Matrices(glm::mat4* proj, glm::mat4* ortho, glm::mat4* view) : 
		projectionMatrix(proj), orthoMatrix(ortho), viewMatrix(view) {}

	const glm::mat4 getProjMatrix() const { return *projectionMatrix; }

	const glm::mat4 getOrthoMatrix() const { return *orthoMatrix; }

	const glm::mat4 getViewMatrix() const { return *viewMatrix; }
};

class Heightmap
{
private:
	const int rows;
	const int columns;
	const int numVertices;
	const float range;
	const float heightLimit;

	std::vector<std::vector<float>> heightData;

	std::vector<std::vector<std::vector<glm::vec3>>> vertexDatas;

	Matrices matrices;

	GLuint vaoH{};
	GLuint vboH{};

	std::vector<GLuint> shaderID;
	Shader shader;
	GLuint programID;

public:

	Heightmap(std::vector<std::vector<float>> mapData, glm::mat4* proj, glm::mat4* ortho, glm::mat4* view) :
		rows(mapData.size()),
		columns(mapData[0].size()),
		numVertices(rows* columns),
		range(-0.5f),
		heightLimit(12.0f),
		heightData(mapData),
		matrices(proj, ortho, view) { initializeHeightmap(); }

	void initializeHeightmap();

	void initializeShaderProgram();


	void attrib();

	void createFromHeightData();

	bool hasPositions();
	bool hasTextureCoord();
	bool hasNormals();

	void setUpVertices();
	void setUpTextureCoordinates();
	void setUpNormals();
	void setUpIndexBuffer();

};