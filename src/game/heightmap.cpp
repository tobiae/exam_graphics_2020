﻿#include <heightmap.h>

void Heightmap::initializeHeightmap()
{
    initializeShaderProgram();
    setUpVertices();
    attrib();
}

void Heightmap::initializeShaderProgram()
{
    shaderID.push_back(shader.compileShaderFromFilepath("resources/shaders/map_vertex.vert", GL_VERTEX_SHADER));
    //shaderID.push_back(shader.compileShaderFromFilepath("resources/shaders/map_fragment.frag", GL_FRAGMENT_SHADER));
    //shaderID.push_back(shader.compileShaderFromFilepath("resources/shaders/map_ambient.frag", GL_FRAGMENT_SHADER));
    //shaderID.push_back(shader.compileShaderFromFilepath("resources/shaders/map_diffuse.frag", GL_FRAGMENT_SHADER));

    shader.createProgram();

}

void Heightmap::createFromHeightData()
{

    shader.bindShaderProgram();

    // Render heightmap
    const auto heightmapModelMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(200.f, 10.f, 200.f)); // heightMapSize

    shader.setMatrix4((GLuint)2, matrices.getProjMatrix());
    shader.setMatrix4((GLuint)1, matrices.getViewMatrix());
    shader.setMatrix4((GLuint)0, heightmapModelMatrix);

    glBindVertexArray(this->vaoH);

    glDrawArrays(GL_TRIANGLES, 0, this->numVertices);
    //

    //unbinds the VAO:
    glBindVertexArray(0);

    /*
    if (hasTextureCoordinates()) {
        setUpTextureCoordinates();
    }

    if (hasNormals())
    {
        if (!hasPositions()) {
            setUpVertices();
        }

        setUpNormals();
    }

    setUpIndexBuffer();

    // Clear the data, we won't need it anymor
    //_vertices.clear();
    //_textureCoordinates.clear();
    //_normals.clear();

    // If get here, we have succeeded with generating heightma
    //_isInitialized = true;
    */

    /*
    glBindVertexArray(vaoH);
    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(_primitiveRestartIndex);

    glDrawElements(GL_TRIANGLE_STRIP, _numIndices, GL_UNSIGNED_INT, 0);
    glDisable(GL_PRIMITIVE_RESTART);
    */
}

void Heightmap::attrib()
{
    /*

    //vertShader = glCreateShader(GL_VERTEX_SHADER​);

    //glShaderSource(GLuint shader​, GLsizei count​, const GLchar * *string​, const GLint * length​);

    // First, prepare VAO and VBO for vertex dat
    glGenVertexArrays(1, &vaoH);
    glBindVertexArray(vaoH);

    glGenBuffers(1, &vboH);
    glBindBuffer(GL_ARRAY_BUFFER, vboH);

    //glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(VertexArray), NULL, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(glm::vec3), NULL, GL_STATIC_DRAW);

    for (int i = 0; i < rows; i++)
    {
        glBufferSubData(GL_ARRAY_BUFFER, 0, rows * sizeof(glm::vec3), vertexDatas[0][i].data());
    }

    //glBufferSubData(GL_ARRAY_BUFFER, 0, rows * sizeof(glm::vec3), vertexDatas[0][x].data());
    //vertexDatas[0][x].data(), columns * sizeof(glm::vec3));
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    */
}

void Heightmap::setUpVertices()
{

    float vertexY;

    vertexDatas = std::vector<std::vector<std::vector<glm::vec3>>> (4, std::vector<std::vector<glm::vec3> >(rows, std::vector<glm::vec3>(columns)));;

    for (auto x = 0; x < rows; x++)
    {
        for (auto z = 0; z < columns; z++)
        {
            const auto factorRow = float(x) / float(rows - 1);
            const auto factorColumn = float(z) / float(columns - 1);
            vertexY = heightData[x][z];
            vertexDatas[0][x][z] = glm::vec3(-0.5f + factorColumn, vertexY, -0.5f + factorRow);
        }
    }
}

/*

void Heightmap::setUpTextureCoordinates()
{
    _textureCoordinates = std::vector<std::vector<glm::vec2>>(_rows, std::vector<glm::vec2>(_columns));

    const auto textureStepU = 0.1f;
    const auto textureStepV = 0.1f;

    for (auto i = 0; i < _rows; i++)
    {
        for (auto j = 0; j < _columns; j++) {
            _textureCoordinates[i][j] = glm::vec2(textureStepU * j, textureStepV * i);
        }
        _vbo.addData(_textureCoordinates[i].data(), _columns * sizeof(glm::vec2));
    }
}

void Heightmap::setUpNormals()
{
    _normals = std::vector<std::vector<glm::vec3>>(_rows, std::vector<glm::vec3>(_columns));
    std::vector< std::vector<glm::vec3> > tempNormals[2];
    for (auto i = 0; i < 2; i++) {
        tempNormals[i] = std::vector<std::vector<glm::vec3>>(_rows - 1, std::vector<glm::vec3>(_columns - 1));
    }

    for (auto i = 0; i < _rows - 1; i++)
    {
        for (auto j = 0; j < _columns - 1; j++)
        {
            const auto& vertexA = _vertices[i][j];
            const auto& vertexB = _vertices[i][j + 1];
            const auto& vertexC = _vertices[i + 1][j + 1];
            const auto& vertexD = _vertices[i + 1][j];

            const auto triangleNormalA = glm::cross(vertexB - vertexA, vertexA - vertexD);
            const auto triangleNormalB = glm::cross(vertexD - vertexC, vertexC - vertexB);

            tempNormals[0][i][j] = glm::normalize(triangleNormalA);
            tempNormals[1][i][j] = glm::normalize(triangleNormalB);
        }
    }

    for (auto i = 0; i < _rows; i++)
    {
        for (auto j = 0; j < _columns; j++)
        {
            const auto isFirstRow = i == 0;
            const auto isFirstColumn = j == 0;
            const auto isLastRow = i == _rows - 1;
            const auto isLastColumn = j == _columns - 1;

            auto finalVertexNormal = glm::vec3(0.0f, 0.0f, 0.0f);

            // Look for triangle to the upper-lef
            if (!isFirstRow && !isFirstColumn) {
                finalVertexNormal += tempNormals[0][i - 1][j - 1];
            }

            // Look for triangles to the upper-righ
            if (!isFirstRow && !isLastColumn) {
                for (auto k = 0; k < 2; k++) {
                    finalVertexNormal += tempNormals[k][i - 1][j];
                }
            }

            // Look for triangle to the bottom-righ
            if (!isLastRow && !isLastColumn) {
                finalVertexNormal += tempNormals[0][i][j];
            }

            // Look for triangles to the bottom-righ
            if (!isLastRow && !isFirstColumn) {
                for (auto k = 0; k < 2; k++) {
                    finalVertexNormal += tempNormals[k][i][j - 1];
                }
            }

            // Store final normal of j-th vertex in i-th ro
            _normals[i][j] = glm::normalize(finalVertexNormal);
        }
        _vbo.addData(_normals[i].data(), _columns * sizeof(glm::vec3));
    }
}
*/

/*
void Heightmap::setUpIndexBuffer()
{
   // Create a VBO with heightmap indices
    GLuint indicesVbo;
    glGenBuffers(1, &indicesVbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numVertices * sizeof(glm::vec3), NULL, GL_STATIC_DRAW);
    int primitiveRestartIndex = numVertices;

   for (auto i = 0; i < rows - 1; i++)
   {
      for (auto j = 0; j < columns; j++)
      {
         for (auto k = 0; k < 2; k++)
         {
            const auto row = i + k;
            const auto index = row * columns + j;
            glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(int), &index);
         }
      }
      // Restart triangle strips
      _indicesVBO.addData(&primitiveRestartIndex, sizeof(int) *);
   }

   // Send indices to GPU
   _indicesVBO.uploadDataToGPU(GL_STATIC_DRAW);

   // Calculate total count of indices
   int numIndices = (rows - 1)*columns * 2 + rows - 1;
}

*/

/*

void Heightmap::render() const
{
    if (!_isInitialized) {
        return;
    }

    glBindVertexArray(_vao);
    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(_primitiveRestartIndex);

    glDrawElements(GL_TRIANGLE_STRIP, _numIndices, GL_UNSIGNED_INT, 0);
    glDisable(GL_PRIMITIVE_RESTART);
}

void OpenGLWindow::renderScene()
{
    // ...

    const auto heightmapModelMatrix = glm::scale(glm::mat4(1.0f), heightMapSize);
    mainProgram.setModelAndNormalMatrix(heightmapModelMatrix);
    TextureManager::getInstance().getTexture("clay").bind(0);
    heightmap->render();

    // ...
}

*/