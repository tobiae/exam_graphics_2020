Exam

Overview:
This is a simple application where you move around map og gjovik. You move with WASD-keys and can use shift to move the camera faster.
The world is static, so there are no real interactions except for collision with terrain.
A rotating lightsource (sun) rotates around the world, and it gets dark when the lightsource is on the other side of the hemisphere. 
You can make it brighter by pressing 'G'

Level:
 -> Loads map of gjovik to screen
 -> Have different texture terrain based on the altitude of map

Camera and Light:
 -> first person flying camera
 -> free-moving camera whose orentiation is controlled by mouse/keyboard
 -> Rotating sun (day/night cycle) which lights the scene
 -> Made zoom functionality
 -> Camera has collision detection
 
Objects:
 -> Loaded in tree, and fish objects
