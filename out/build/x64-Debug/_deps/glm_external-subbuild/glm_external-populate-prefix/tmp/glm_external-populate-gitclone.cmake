
if(NOT "C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-subbuild/glm_external-populate-prefix/src/glm_external-populate-stamp/glm_external-populate-gitinfo.txt" IS_NEWER_THAN "C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-subbuild/glm_external-populate-prefix/src/glm_external-populate-stamp/glm_external-populate-gitclone-lastrun.txt")
  message(STATUS "Avoiding repeated git clone, stamp file is up to date: 'C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-subbuild/glm_external-populate-prefix/src/glm_external-populate-stamp/glm_external-populate-gitclone-lastrun.txt'")
  return()
endif()

execute_process(
  COMMAND ${CMAKE_COMMAND} -E remove_directory "C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-src"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to remove directory: 'C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-src'")
endif()

# try the clone 3 times in case there is an odd git clone issue
set(error_code 1)
set(number_of_tries 0)
while(error_code AND number_of_tries LESS 3)
  execute_process(
    COMMAND "C:/Program Files/Git/cmd/git.exe"  clone --progress "https://github.com/g-truc/glm.git" "glm_external-src"
    WORKING_DIRECTORY "C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps"
    RESULT_VARIABLE error_code
    )
  math(EXPR number_of_tries "${number_of_tries} + 1")
endwhile()
if(number_of_tries GREATER 1)
  message(STATUS "Had to git clone more than once:
          ${number_of_tries} times.")
endif()
if(error_code)
  message(FATAL_ERROR "Failed to clone repository: 'https://github.com/g-truc/glm.git'")
endif()

execute_process(
  COMMAND "C:/Program Files/Git/cmd/git.exe"  checkout 9749727c2db4742369219e1d452f43e918734b4e --
  WORKING_DIRECTORY "C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-src"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to checkout tag: '9749727c2db4742369219e1d452f43e918734b4e'")
endif()

execute_process(
  COMMAND "C:/Program Files/Git/cmd/git.exe"  submodule update --recursive --init 
  WORKING_DIRECTORY "C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-src"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to update submodules in: 'C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-src'")
endif()

# Complete success, update the script-last-run stamp file:
#
execute_process(
  COMMAND ${CMAKE_COMMAND} -E copy
    "C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-subbuild/glm_external-populate-prefix/src/glm_external-populate-stamp/glm_external-populate-gitinfo.txt"
    "C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-subbuild/glm_external-populate-prefix/src/glm_external-populate-stamp/glm_external-populate-gitclone-lastrun.txt"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to copy script-last-run stamp file: 'C:/Users/tobia/Documents/grafikkProg/eksamensøving/imt2531_assignment_2/out/build/x64-Debug/_deps/glm_external-subbuild/glm_external-populate-prefix/src/glm_external-populate-stamp/glm_external-populate-gitclone-lastrun.txt'")
endif()

